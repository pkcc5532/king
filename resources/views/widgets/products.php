<div class="container products" >
	<h4 class="pull-left">맛집 전체 검색</h4>
    <a class="theme-btn pull-right smooth item-list" data-title="<?=translate('Products');?>" href="products">더보기 &#8594;</a>
	<div class="clearfix"></div>
	<div id="products" class="product-container">
		<?php
			$products = DB::select("SELECT * FROM products ORDER BY id DESC LIMIT 4");
			foreach($products as $product){
				echo '<div class="col-md-3">
                    <a href="product/'.path($product->title,$product->id).'" data-title="'.translate($product->title).'" class="smooth item-hover">
                        <div class="product" id="'.$product->id.'">
                            <div class="pi" style="background-image:url('.image_order($product->oneimage).');"></div>
                            <div class="item-inner-box">
                                <div class="item-title">'.translate($product->title).'</div>
                                <div>'.translate($product->addr1).'</div>
                                <div>'.translate($product->ftype).'</div>
                            </div>
                        </div>
                        <div class="text-center item-grade">
                            <span class="item-grade-title">평점</span>
                            <span class="item-grade-num">'.$product->score.'</span>
                        </div>
					</a>
				</div>';
			}
		?>
		<div class="clearfix"></div>
	</div>
</div>
<div class="parallax" style="background-color: #2e3141;background-image: linear-gradient(to top, rgba(46, 49, 65, 0.8), rgba(46, 49, 65, 0.8)), url(&quot;assets/bg.jpg&quot;);">
    <div class="container">
        <div class="parallax-inner-wrap">
            <div class="parallax-inner-title text-center">PEVERR 리얼 리뷰</div>
            <!-- Swiper -->
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <a href="#"><img src="https://i.ytimg.com/vi/i--vkrK7Tsg/hqdefault.jpg?sqp=-oaymwEXCPYBEIoBSFryq4qpAwkIARUAAIhCGAE=&rs=AOn4CLDv_F8pk3aYR7OFpWEnVDzV51v3cg"></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="#"><img src="https://i.ytimg.com/vi/hHnRZlYr8_E/hqdefault.jpg?sqp=-oaymwEXCPYBEIoBSFryq4qpAwkIARUAAIhCGAE=&rs=AOn4CLAoqpAygO3VZZg1Bb8ckO-6DyeoBA"></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="#"><img src="https://i.ytimg.com/vi/iwmkQ_UZ0T4/hqdefault.jpg?sqp=-oaymwEXCPYBEIoBSFryq4qpAwkIARUAAIhCGAE=&rs=AOn4CLAwOCvQ5vf-zRRsO4zdjOH3PdZnxw"></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="#"><img src="https://i.ytimg.com/vi/QZ2rFP1VGmM/hqdefault.jpg?sqp=-oaymwEXCPYBEIoBSFryq4qpAwkIARUAAIhCGAE=&rs=AOn4CLB711QoPgx8Hzu6iuDUkoZ690Mhlg"></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="#"><img src="https://i.ytimg.com/vi/0rydwHuY_l0/hqdefault.jpg?sqp=-oaymwEXCPYBEIoBSFryq4qpAwkIARUAAIhCGAE=&rs=AOn4CLCy1Y8zhr1FT7zIrkM0obufefVz5w"></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="#"><img src="https://i.ytimg.com/vi/TXUH7GUdChg/hqdefault.jpg?sqp=-oaymwEXCPYBEIoBSFryq4qpAwkIARUAAIhCGAE=&rs=AOn4CLCwt98yU5pUpAQWeXNOJi-ucAKrsQ"></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="#"><img src="https://i.ytimg.com/vi/X-anAJv81Is/hqdefault.jpg?sqp=-oaymwEXCPYBEIoBSFryq4qpAwkIARUAAIhCGAE=&rs=AOn4CLDOW7pEFvzuBicYR-F7VbAEqkqd7A"></a>
                    </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
            </div>
        </div>
        <script>
            var swiper = new Swiper('.swiper-container', {
                width:1000,
                slidesPerView: 3,
                spaceBetween: 30,
                freeMode: true,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
            });
        </script>
    </div>
</div>
<div class="container products" >
    <h4 class="pull-left">이벤트 참가</h4>
    <a class="theme-btn pull-right smooth item-list" data-title="<?=translate('Products');?>" href="products">더보기 &#8594;</a>
    <div class="clearfix"></div>
    <div id="products" class="product-container">
        <div class="col-md-3">
            <a href="#">
                <div class="product product-event">
                    <div class="pi" style="background-image:url(https://s3-ap-northeast-1.amazonaws.com/file1.weble.net/campaign/data/129323/thumb200.jpg?bust=1512466718478);"></div>
                    <div class="item-inner-box">
                        <div class="item-title">보나베티 일산점</div>
                        <div>다양한 놀거리가 있는 키즈카페!</div>
                        <div>엄마도, 아이도 즐거운 키즈카페</div>
                    </div>
                    <b class="item-price">무료이용권</b>
                    <div class="text-center item-grade">
                        <span class="item-grade-title">신청</span>
                        <span class="item-grade-num">0/10</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="#">
                <div class="product product-event">
                    <div class="pi" style="background-image:url(https://s3-ap-northeast-1.amazonaws.com/file1.weble.net/campaign/data/129271/thumb200.jpg?bust=1512466883968);"></div>
                    <div class="item-inner-box">
                        <div class="item-title">[방탄필름]트라움가드 갤럭시S8플러스</div>
                        <div>대한민국 대표 기적의 방탄액정보호필름</div>
                    </div>
                    <b class="item-price">방탄필름 갤럭시 S8 플러스 1개 제공</b>
                    <div class="text-center item-grade">
                        <span class="item-grade-title">신청</span>
                        <span class="item-grade-num">10/10</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="#">
                <div class="product product-event">
                    <div class="pi" style="background-image:url(https://s3-ap-northeast-1.amazonaws.com/file1.weble.net/campaign/data/129241/thumb200.jpg?bust=1512467103510);"></div>
                    <div class="item-inner-box">
                        <div class="item-title">[도서] 코리안드림</div>
                        <div>대한민국 출판문화예술대상 올해의 책 선정 도서</div>
                    </div>
                    <b class="item-price">코리안드림 1권</b>
                    <div class="text-center item-grade">
                        <span class="item-grade-title">신청</span>
                        <span class="item-grade-num">3/5</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="#">
                <div class="product product-event">
                    <div class="pi" style="background-image:url(https://s3-ap-northeast-1.amazonaws.com/file1.weble.net/campaign/data/129023/thumb200.jpg?bust=1512467263682);"></div>
                    <div class="item-inner-box">
                        <div class="item-title">반반한족발 여수학동점</div>
                        <div>쫄깃하고 고소하고 달콤한 족발의 만남!!
                            반반한족발에서 행복한 식감을 선사합니다.</div>
                    </div>
                    <b class="item-price">3만원 자유이용권</b>
                    <div class="text-center item-grade">
                        <span class="item-grade-title">신청</span>
                        <span class="item-grade-num">1/5</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="#">
                <div class="product product-event">
                    <div class="pi" style="background-image:url(https://s3-ap-northeast-1.amazonaws.com/file1.weble.net/campaign/data/129323/thumb200.jpg?bust=1512466718478);"></div>
                    <div class="item-inner-box">
                        <div class="item-title">보나베티 일산점</div>
                        <div>다양한 놀거리가 있는 키즈카페!</div>
                        <div>엄마도, 아이도 즐거운 키즈카페</div>
                    </div>
                    <b class="item-price">무료이용권</b>
                    <div class="text-center item-grade">
                        <span class="item-grade-title">신청</span>
                        <span class="item-grade-num">0/10</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="#">
                <div class="product product-event">
                    <div class="pi" style="background-image:url(https://s3-ap-northeast-1.amazonaws.com/file1.weble.net/campaign/data/129271/thumb200.jpg?bust=1512466883968);"></div>
                    <div class="item-inner-box">
                        <div class="item-title">[방탄필름]트라움가드 갤럭시S8플러스</div>
                        <div>대한민국 대표 기적의 방탄액정보호필름</div>
                    </div>
                    <b class="item-price">방탄필름 갤럭시 S8 플러스 1개 제공</b>
                    <div class="text-center item-grade">
                        <span class="item-grade-title">신청</span>
                        <span class="item-grade-num">10/10</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="#">
                <div class="product product-event">
                    <div class="pi" style="background-image:url(https://s3-ap-northeast-1.amazonaws.com/file1.weble.net/campaign/data/129241/thumb200.jpg?bust=1512467103510);"></div>
                    <div class="item-inner-box">
                        <div class="item-title">[도서] 코리안드림</div>
                        <div>대한민국 출판문화예술대상 올해의 책 선정 도서</div>
                    </div>
                    <b class="item-price">코리안드림 1권</b>
                    <div class="text-center item-grade">
                        <span class="item-grade-title">신청</span>
                        <span class="item-grade-num">3/5</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="#">
                <div class="product product-event">
                    <div class="pi" style="background-image:url(https://s3-ap-northeast-1.amazonaws.com/file1.weble.net/campaign/data/129023/thumb200.jpg?bust=1512467263682);"></div>
                    <div class="item-inner-box">
                        <div class="item-title">반반한족발 여수학동점</div>
                        <div>쫄깃하고 고소하고 달콤한 족발의 만남!!
                            반반한족발에서 행복한 식감을 선사합니다.</div>
                    </div>
                    <b class="item-price">3만원 자유이용권</b>
                    <div class="text-center item-grade">
                        <span class="item-grade-title">신청</span>
                        <span class="item-grade-num">1/5</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
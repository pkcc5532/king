<?php echo $header?>
    <?php
    setlocale(LC_ALL,'ko_KR.UTF-8')
?>
<!-- test 1-->
    <!-- test 2-->
<!-- test 3 -->


    <div class="search-wrap">
        <div class="container">
            <form id="search">
                <div class="col-md-7">
                    <div class="form-group">
                        <input name="search" placeholder="<?=translate('Search keyword')?>, 주소, 전화번호, 음식종류" type="text" value="<?=isset($_GET['search'])?$_GET['search']:''?>" class="form-control" />
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-price">
                        <div class="clearfix"></div>
                        <b class="pull-left"><?=$price['min'] ?></b>
                        <b class="pull-right"><?=$price['max']; ?></b>
                        <input name="min" id="min" type="hidden">
                        <input name="max" id="max" type="hidden">
                        <div id="price"></div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group text-center">
                        <button type="submit" class="btn-search search-btn"><?=translate('Search')?></button>
                    </div>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
	<div class="container">
		<div id="listing" class="product-container row space-margin">
			<?php
            foreach($events as $event){
                echo '<div class="col-md-3">
                        <a href="product/'.path($event->title,$event->id).'" data-title="'.translate($event->title).'" class="smooth item-hover">
                            <div class="product" id="'.$event->id.'">
                                <div class="pi" style="background-image:url('.url('/assets/products/'.image_order($event->images)).');"></div>
                                <div class="item-inner-box">
                                    <div class="item-title">'.translate($event->title).'</div>
                                    <div>매일 11:30~ 21:30</div>
                                    <div>청담동 · 서울특별시 강남구 청담동 129-10</div>
                                    <div>꽃등심, 육회, 육회비빔밥</div>
                                </div>
                                <b class="item-price">'.c($event->price).'</b>
                            </div>
                            <div class="text-center item-grade">
                                <span class="item-grade-title">평점</span>
                                <span class="item-grade-num">8.0</span>
                            </div>
                            <div class="bg view">
                                <h5>'.translate($event->title).'</h5>
                                <p>'.mb_substr(translate($event->text),0,200).'</p>
                                <a href="product/'.$event->id.'-'.$event->title.'" data-title="'.translate($event->title).'" class="smooth">
                                    <i class="icon-eye"></i>
                                    Details
                                </a>
                            </div>
						</a>
					</div>';
            }
			?>
			<div class="clearfix"></div>
		</div>
	</div>
<script>
		var handlesSlider = document.getElementById('price');
		noUiSlider.create(handlesSlider, {
			start: [<?=$price['min']?>,<?=$price['max']?>],
			step: 1,
			connect: false,
			range: {'min':<?=$price['min']?>,'max':<?=$price['max']?>},
		});
		var BudgetElement = [document.getElementById('min'),document.getElementById('max')];
		handlesSlider.noUiSlider.on('update', function(values, handle) {
			BudgetElement[0].textContent = values[0];
			BudgetElement[1].textContent = values[1];
			$("#min").val(values[0]);
			$(".pull-left.price").html(values[0]);
			$("#max").val(values[1]);
			$(".pull-right.price").html(values[1]);
		});
</script>
<?php echo $footer?>